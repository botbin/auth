package main

import (
	"os"
	"strings"

	"gitlab.com/botbin/auth/pkg/event"
	"gitlab.com/botbin/auth/pkg/web"
	"go.uber.org/zap"
)

func init() {
	l, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	zap.ReplaceGlobals(l)
}

const (
	app     = "auth"
	version = "0.12.5"
)

func main() {
	zap.S().Infof("started %s v%s", app, version)

	err := event.UseKafkaProducer(strings.Split(os.Getenv("KAFKA_BROKERS"), ","))
	if err != nil {
		zap.S().Panicw("could not create kafka producer", "error", err)
	}

	server, err := web.NewServer()
	if err != nil {
		zap.S().Panicw("failed to start server", "error", err)
	}

	if err := server.Start("8080"); err != nil {
		zap.S().Errorw("encountered server error", "error", err)
	}
}
