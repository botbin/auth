# Auth
This service handles cluster authentication and identity management.
It decouples other microservices from the Discord-specific aspects of
token management and allows for effective revocation of refresh/access
tokens. The main API docs cover public aspects of this service, so
this document mainly covers operation and microservice interaction.

## Contents
- [Principals](#principals)
- [Identities](#identity-management)
- [Token Invalidation](#token-invalidation)
- [Data Model](#data-model)
- [Environment Variables](#environment-variables)

## Principals
Each access token conveys information about a principal. Thus, token
management begins with creating a principal and its initial identity.

A principal can be created by sending a POST request to `/v1/principals`,
providing a `code` query parameter and any key/value pairs that will
form the initial identity. `code` corresponds to an authorization code
from the Discord OAuth2 flow.

For example, a users service might make the following call to create
a new account:

```
POST https://authorization/v1/principals?code=oneTimeCode&username=jane
```

The response format is the same as defined for refresh token creation
If the code corresponds to a Discord account that is already paired
with a principal, the identities will not be applied but the response
will still contain a valid token pair.

## Identities
Principals may have key/value pairs associated with them. The collection
of entries forms their identity and will be present as claims in created
access tokens. Internal services can alter identities through RESTful
requests.

### Create New Entries
New entries can be added to the identity table by issuing a POST request
to `/v1/identity`, providing the key/value pairs as query parameters.
The `principal` parameter is required in order to identify where the
changes will be applied. The request will fail if a provided key already
exists.

### Update an Existing Entry
Existing entries can be updated by issuing a PUT request to `/v1/identity`,
providing key/value pairs as query parameters. The `principal` parameter is
required in order to identify where the changes will be applied.

### Delete an Entry
Entries can be removed by issuing a DELETE request to `/v1/identity`,
providing key/value pairs as query parameters. The `principal` parameter is
required in order to identify where the changes will be applied. When a
principal has an entry removed, all existing access tokens become invalid.

## Token Invalidation
Refresh tokens may be revoked through the `/v1/tokens/revoked` endpoint.
This causes both the refresh token and all access tokens it created to
become invalid. The main API docs cover revoking tokens; here, we will
cover identifying them.

### Format
Revoked tokens use two properties to identify the JWT they refer to and
indicate when the reference can be discarded. They are JSON objects with
the following format:
```json
{
    "rid": string,
    "expiresAt": int
}
```
`rid` matches the `rid` claim found in the JWTs. It provides a way of
quickly determining if a token was revoked. `expiresAt` is a Unix
timestamp that indicates when the JWT will expire. It matches the `exp`
claim of the JWT.

### Retrieval
The authorization service can emit revoked tokens in two ways: in direct
response to a REST call and by publishing events to Kafka.

#### REST Interface
Sending a GET request to `/v1/tokens/revoked` will retrieve an array of all unexpired
revoked tokens. If none exist, an empty array is returned. This method is not exposed
in the gateway configuration, so it is not reachable through the API gateway.

#### Kafka
As tokens are revoked, they are published to the `tokens.revoked` Kafka topic.
The message body follows the same format as the REST interface, but the contents
will not contain the full list of tokens. To ensure a complete local list is kept,
a service should begin consuming the topic and then fetch the full list.

## Data Model
The service interacts with three data stores, though the majority of
data access is only with two.

### PostgreSQL
PostgreSQL keeps a collection of principal IDs and their associated
Discord ID. Principals are what end up in the `sub` claim of access
tokens and require strong persistence guarantees that Redis cannot
efficiently provide. This store will rarely be accessed.

### Redis
Redis stores all data that must persist between service instances. This
includes the principal data that is found in PostgreSQL.

It is important to note that it is not used as a cache. Production Redis
instances should use an AOF persistence policy of ideally one second.
In the event of a Redis restart, postgres will keep the absolutely
necessary information that will then be fed back into Redis on demand.

### Kafka
Kafka is used to publish auth-related events such as the revocation of
access tokens. This readme details the topic names and data formats.

## Environment Variables
- DISCORD_CLIENT_ID
- DISCORD_CLIENT_SECRET
- DISCORD_REDIRECT_URI - This should match the redirect URI used to create the initial Discord OAuth2 code
- REDIS_URL - A connection string for Redis
- KAFKA_BROKERS - A comma-separated list of Kafka brokers
- POSTGRES_URL - A connection string for PostgreSQL
- JWT_KEY - The signing secret for JWT creation and validation