package data

import (
	"github.com/go-redis/redis"
	"gitlab.com/botbin/auth/pkg/sign"
)

// TokenStore types model storage for token-related data.
type TokenStore interface {
	StoreNewTokenSet(string, string, int, sign.Token) error
	GetRefreshTokenDetails(string) (RefreshTokenMeta, error)
	StoreAccessToken(int, string, sign.Token) error
	RemoveRefreshToken(string) ([]redis.Z, error)
	ToRevokedTokens(entries []redis.Z) []sign.RevokedToken
	StoreRevokedAccessTokens([]redis.Z) error
	GetRevokedTokens() []sign.RevokedToken
	RemoveAccessTokens(principal string) ([]redis.Z, error)
}

// RefreshTokenMeta models metadata surrounding a cluster refresh token.
type RefreshTokenMeta struct {
	// Principal is the principal who created the token.
	Principal int

	// DiscordRefreshToken is the Discord refresh token that was used to
	// create the cluster refresh token.
	DiscordRefreshToken string
}
