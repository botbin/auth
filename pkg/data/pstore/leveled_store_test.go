package pstore_test

import (
	"os"
	"testing"

	"github.com/satori/go.uuid"
	"gitlab.com/botbin/auth/pkg/data"
	"gitlab.com/botbin/auth/pkg/data/pstore"

	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

// PrincipalStoreTestSuite tests the default PrincipalStore implementation.
type PrincipalStoreTestSuite struct {
	suite.Suite
	cache    *redis.Client
	postgres *sqlx.DB
}

func (ps *PrincipalStoreTestSuite) SetupSuite() {
	ps.cache = data.GetRedisClient()
	assert.Nil(ps.T(), ps.cache.Ping().Err())

	ps.postgres = data.GetPGConn()
	assert.Nil(ps.T(), ps.postgres.Ping())

	ps.clearEnv()
}

func (ps *PrincipalStoreTestSuite) TearDownTest() {
	ps.clearEnv()
}

func (ps *PrincipalStoreTestSuite) clearEnv() {
	err := ps.cache.Del(pstore.CacheKey).Err()
	assert.Nil(ps.T(), err)

	_, err = ps.postgres.Exec("DELETE FROM " + pstore.SQLTable)
	assert.Nil(ps.T(), err)
}

func (ps *PrincipalStoreTestSuite) TestCreate() {
	store, err := pstore.New(ps.cache, ps.postgres)
	assert.Nil(ps.T(), err)

	did := uuid.NewV4().String()
	pid, err := store.Create(did)
	assert.Nil(ps.T(), err)
	assert.NotEmpty(ps.T(), pid)

	err = ps.cache.Exists(pstore.CacheKey).Err()
	assert.Nil(ps.T(), err)

	count := 0
	err = ps.postgres.QueryRow("SELECT COUNT(*) FROM " + pstore.SQLTable).Scan(&count)
	assert.Nil(ps.T(), err)
	assert.Equal(ps.T(), 1, count)
}

func (ps *PrincipalStoreTestSuite) TestGet() {
	store, err := pstore.New(ps.cache, ps.postgres)
	assert.Nil(ps.T(), err)

	did := uuid.NewV4().String()
	pid, err := store.Create(did)
	assert.Nil(ps.T(), err)
	assert.NotEmpty(ps.T(), pid)

	dbPID, err := store.Get(did)
	assert.Nil(ps.T(), err)
	assert.Equal(ps.T(), pid, dbPID)

	err = ps.cache.Del(pstore.CacheKey).Err()
	assert.Nil(ps.T(), err)

	dbPID, err = store.Get(did)
	assert.Nil(ps.T(), err)
	assert.Equal(ps.T(), pid, dbPID)
}

func TestPrincipalStore(t *testing.T) {
	if os.Getenv("TEST_MODE") == "all" {
		suite.Run(t, new(PrincipalStoreTestSuite))
	}
}
