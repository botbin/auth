package pstore

// A PrincipalStore provides access to principals.
type PrincipalStore interface {
	Create(string) (int, error)
	Get(string) (int, error)
}
