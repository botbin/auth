package pstore

import (
	"database/sql"
	"fmt"
	"strconv"

	"github.com/go-redis/redis"
	"github.com/jmoiron/sqlx"
)

type leveledStore struct {
	cache *redis.Client
	db    *sqlx.DB

	insertPrincipal *sql.Stmt
	getPrincipal    *sql.Stmt
}

const (
	// CacheKey is the key used for caching principal data.
	CacheKey = "principalsByDiscordId"
	// SQLTable is the SQL table used for storing principal data.
	SQLTable = "auth.principals"
)

// New creates a PrincipalStore that stores data in both a cache
// layer and a SQL database. During reads, it will consult the
// cache first, defaulting to the SQL store in rare cases.
func New(cache *redis.Client, db *sqlx.DB) (PrincipalStore, error) {
	insertPrincipal, err := db.Prepare(fmt.Sprintf(`
		INSERT INTO %s (discord_id)
		VALUES ($1)
		RETURNING id
	`, SQLTable))
	if err != nil {
		return nil, err
	}

	getPrincipal, err := db.Prepare(fmt.Sprintf(`
		SELECT id FROM %s
		WHERE discord_id = $1
	`, SQLTable))
	if err != nil {
		return nil, err
	}

	return &leveledStore{
		cache:           cache,
		db:              db,
		insertPrincipal: insertPrincipal,
		getPrincipal:    getPrincipal,
	}, nil
}

// Create creates a new principal and returns their ID.
func (ls *leveledStore) Create(discordID string) (int, error) {
	principal, err := ls.storeDBPrincipal(discordID)
	if err != nil {
		return -1, err
	}
	return principal, ls.cachePrincipal(principal, discordID)
}

func (ls *leveledStore) cachePrincipal(principal int, discordID string) error {
	_, err := ls.cache.HSet(CacheKey, discordID, principal).Result()
	return err
}

func (ls *leveledStore) storeDBPrincipal(discordID string) (int, error) {
	var id int
	err := ls.insertPrincipal.QueryRow(discordID).Scan(&id)
	return id, err
}

// Get retrieves the ID of the principal with the given Discord ID.
// A non-nil error is returned if the principal does not exist.
func (ls *leveledStore) Get(discordID string) (int, error) {
	if principal, err := ls.getCachedPrincipal(discordID); err == nil {
		return principal, nil
	}
	return ls.getDBPrincipal(discordID)
}

func (ls *leveledStore) getCachedPrincipal(discordID string) (int, error) {
	res, err := ls.cache.HGet(CacheKey, discordID).Result()
	if err != nil {
		return -1, err
	}
	return strconv.Atoi(res)
}

func (ls *leveledStore) getDBPrincipal(discordID string) (int, error) {
	var id int
	err := ls.getPrincipal.QueryRow(discordID).Scan(&id)
	return id, err
}
