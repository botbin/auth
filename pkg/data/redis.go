package data

import (
	"errors"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/botbin/auth/pkg/sign"
	"go.uber.org/zap"
)

var (
	redisClient     *redis.Client
	redisClientOnce sync.Once
)

// GetRedisClient returns a cached Redis client that is safe for concurrent
// use by multiple goroutines.
func GetRedisClient() *redis.Client {
	redisClientOnce.Do(func() {
		url := GetEnv("REDIS_URL", "redis://localhost:6379", false)
		opts, err := redis.ParseURL(url)
		if err != nil {
			zap.S().Errorw("could not parse Redis URL", "error", err)
			panic(err)
		}

		redisClient = redis.NewClient(opts)
	})
	return redisClient
}

// RedisTokenStore is a TokenStore that uses Redis as a storage backing.
type RedisTokenStore struct {
	Client *redis.Client
}

const (
	// StaleRefreshTokenDuration is the time that unused refresh tokens will
	// remain in the database.
	StaleRefreshTokenDuration = time.Hour * 24 * 180
)

// NewRedisTokenStore configures a production-ready token store backed by Redis.
func NewRedisTokenStore() TokenStore {
	return RedisTokenStore{
		Client: GetRedisClient(),
	}
}

// StoreNewTokenSet stores a new refresh token and its initial associated tokens.
//
// Refresh tokens that have not been used in more than the time specified by
// StaleRefreshTokenDuration will be removed from the system. The associated
// access token will last for the time specified by AccessTokenDuration.
func (rts RedisTokenStore) StoreNewTokenSet(clusterRefreshToken, discordRefreshToken string, principal int, clusterAccessToken sign.Token) error {
	pipe := rts.Client.TxPipeline()

	refreshTokenKey := "refreshToken:" + clusterRefreshToken
	pipe.HMSet(refreshTokenKey, map[string]interface{}{
		"principal":           principal,
		"discordRefreshToken": discordRefreshToken,
	})
	pipe.Expire(refreshTokenKey, StaleRefreshTokenDuration)

	refreshTokensKey := "refreshTokens:" + strconv.Itoa(principal)
	now := time.Now().UTC()
	pipe.ZRemRangeByScore(refreshTokensKey, "0", strconv.FormatInt(now.Unix(), 10))
	pipe.ZAdd(refreshTokensKey, redis.Z{
		Score:  float64(now.Add(StaleRefreshTokenDuration).Unix()),
		Member: clusterRefreshToken,
	})

	revokeIdsKey := "revokeIds:" + clusterRefreshToken
	pipe.ZAdd(revokeIdsKey, redis.Z{
		Score:  float64(clusterAccessToken.Expires),
		Member: clusterAccessToken.RevokeID,
	})
	pipe.Expire(revokeIdsKey, StaleRefreshTokenDuration)

	_, err := pipe.Exec()
	return err
}

// GetRefreshTokenDetails fetches data surrounding a refresh token and resets its
// expiration date.
func (rts RedisTokenStore) GetRefreshTokenDetails(clusterRefreshToken string) (RefreshTokenMeta, error) {
	pipe := rts.Client.Pipeline()

	refreshTokenKey := "refreshToken:" + clusterRefreshToken
	hashOp := pipe.HGetAll(refreshTokenKey)
	pipe.Expire(refreshTokenKey, StaleRefreshTokenDuration)

	if _, err := pipe.Exec(); err != nil {
		return RefreshTokenMeta{}, err
	}

	hash, err := hashOp.Result()
	if err != nil {
		return RefreshTokenMeta{}, err
	}

	p, _ := strconv.Atoi(hash["principal"])
	meta := RefreshTokenMeta{
		Principal:           p,
		DiscordRefreshToken: hash["discordRefreshToken"],
	}

	if meta.DiscordRefreshToken == "" {
		return meta, errors.New("unrecognized cluster refresh token")
	}
	return meta, nil
}

// StoreAccessToken places an access token in the store as a child of a refresh token.
func (rts RedisTokenStore) StoreAccessToken(principal int, clusterRefreshToken string, clusterAccessToken sign.Token) error {
	revokeIdsKey := "revokeIds:" + clusterRefreshToken
	pipe := rts.Client.TxPipeline()

	pipe.ZRemRangeByScore(revokeIdsKey, "0", strconv.FormatInt(time.Now().UTC().Unix(), 10))
	pipe.ZAdd(revokeIdsKey, redis.Z{
		Score:  float64(clusterAccessToken.Expires),
		Member: clusterAccessToken.RevokeID,
	})
	pipe.Expire(revokeIdsKey, StaleRefreshTokenDuration)

	refreshKey := "refreshTokens:" + strconv.Itoa(principal)
	now := time.Now().UTC().Unix()
	pipe.ZRemRangeByScore(refreshKey, "0", strconv.FormatInt(now, 10))

	_, err := pipe.Exec()
	return err
}

// RemoveRefreshToken removes the refresh token and all access tokens it helped create.
// It will return the collection of removed access tokens.
func (rts RedisTokenStore) RemoveRefreshToken(clusterRefreshToken string) ([]redis.Z, error) {
	principal, err := rts.Client.HGet("refreshToken:"+clusterRefreshToken, "principal").Result()
	if err != nil {
		return nil, err
	}

	pipe := rts.Client.TxPipeline()
	pipe.Del("refreshToken:" + clusterRefreshToken)
	pipe.ZRem("refreshTokens:"+principal, clusterRefreshToken)

	revokeIdsKey := "revokeIds:" + clusterRefreshToken
	removedAccessTokens := pipe.ZRangeWithScores(revokeIdsKey, 0, -1)
	pipe.Del(revokeIdsKey)

	if _, err := pipe.Exec(); err != nil {
		return nil, err
	}
	return removedAccessTokens.Val(), nil
}

// RemoveAccessTokens removes all access tokens that belong to a principal.
// Unlike RemoveRefreshToken, this does not discard any refresh token information.
func (rts RedisTokenStore) RemoveAccessTokens(principal string) ([]redis.Z, error) {
	tokensKey := "refreshTokens:" + principal
	refreshTokens, err := rts.Client.ZRange(tokensKey, 0, -1).Result()

	pipe := rts.Client.TxPipeline()

	var results []*redis.ZSliceCmd
	for _, token := range refreshTokens {
		key := "revokeIds:" + token
		results = append(results, pipe.ZRangeWithScores(key, 0, -1))
		pipe.Del(key)
	}

	_, err = pipe.Exec()
	if err != nil {
		return nil, err
	}

	var ids []redis.Z
	for _, res := range results {
		ids = append(ids, res.Val()...)
	}
	return ids, nil
}

// StoreRevokedAccessTokens stores a collection of access tokens whose parent refresh token
// was revoked.
func (rts RedisTokenStore) StoreRevokedAccessTokens(revokedAccessTokens []redis.Z) error {
	pipe := rts.Client.TxPipeline()

	pipe.ZAdd("revokedIds", revokedAccessTokens...)
	pipe.ZRemRangeByScore("revokedIds", "0", strconv.FormatInt(time.Now().UTC().Unix(), 10))

	_, err := pipe.Exec()
	return err
}

// GetRevokedTokens gets all revoked tokens that have not expired.
// This method will not return nil.
func (rts RedisTokenStore) GetRevokedTokens() []sign.RevokedToken {
	pipe := rts.Client.TxPipeline()

	pipe.ZRemRangeByScore("revokedIds", "0", strconv.FormatInt(time.Now().UTC().Unix(), 10))
	rangeOp := pipe.ZRangeWithScores("revokedIds", 0, -1)

	_, err := pipe.Exec()
	if err != nil {
		return []sign.RevokedToken{}
	}

	res, err := rangeOp.Result()
	if err != nil {
		return []sign.RevokedToken{}
	}
	return rts.ToRevokedTokens(res)
}

// ToRevokedTokens converts a set of revoked id entries to revoked tokens.
func (rts RedisTokenStore) ToRevokedTokens(entries []redis.Z) []sign.RevokedToken {
	tokens := []sign.RevokedToken{}
	for _, setEl := range entries {
		tokens = append(tokens, sign.RevokedToken{
			ID:      setEl.Member.(string),
			Expires: int(setEl.Score),
		})
	}
	return tokens
}
