package istore

import (
	"encoding/json"
	"strconv"
)

// A SerializedIdentity is an Identity whose values are in a canonical form.
type SerializedIdentity map[string]interface{}

func serialize(raw map[string]string) map[string]interface{} {
	serialized := make(map[string]interface{})

	for key, val := range raw {
		serialized[key] = convert(val)
	}
	return serialized
}

// An Identity provides details about a principal.
type Identity map[string]interface{}

// ToIdentity converts a raw identity map (as retrieved from an HTTP request's
// URL query) to an Identity. This allows the identity to preserve original
// type information instead of converting all values to strings.
//
// In the resulting identity, each key may map to one of the following:
//		- A string
//			"yourKey" => "test"
//		- A bool
//			"theirKey" => false
//		- A float64
//			"herKey" => 37.6
//		- An int64
//			"hisKey" => 32
//		- An array of string/bool/int64/float64
//			"myKey" => ["test", 32, false, 37.6]
//
func ToIdentity(raw map[string][]string) Identity {
	id := make(map[string]interface{})
	for key, values := range raw {
		id[key] = marshal(values)
	}
	return id
}

func marshal(vals []string) interface{} {
	if len(vals) == 1 {
		return convert(vals[0])
	}

	var s []interface{}
	for i := range vals {
		s = append(s, convert(vals[i]))
	}
	out, _ := json.Marshal(s)
	return string(out)
}

func convert(val string) interface{} {
	if val == "true" {
		return true
	} else if val == "false" {
		return false
	}

	if i, err := strconv.ParseInt(val, 10, 64); err == nil {
		return i
	}
	if f, err := strconv.ParseFloat(val, 64); err == nil {
		return f
	}

	var vals []interface{}
	err := json.Unmarshal([]byte(val), &vals)
	if err != nil {
		return val
	}
	converted := make([]interface{}, len(vals))
	for i := range vals {
		converted[i] = convert(vals[i].(string))
	}
	return converted
}
