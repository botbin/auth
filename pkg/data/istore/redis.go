package istore

import (
	"errors"

	"github.com/go-redis/redis"
)

type redisIStore struct {
	client *redis.Client
}

// New creates an IdentityStore that uses Redis for backing storage.
func New(client *redis.Client) IdentityStore {
	return &redisIStore{
		client: client,
	}
}

func (rs *redisIStore) Set(principal string, identity Identity) error {
	return rs.client.HMSet(rs.hash(principal), identity).Err()
}

func (rs *redisIStore) SetNX(principal string, identity Identity) error {
	wasSet, err := rs.setNX(principal, identity)
	if err != nil {
		return err
	}

	for key, res := range wasSet {
		// We don't use the error-checking method because any errors in
		// the transaction were caught when calling Exec.
		if !res.Val() {
			return errors.New("duplicate key '" + key + "' cannot be rewritten with this method")
		}
	}
	return nil
}

func (rs *redisIStore) setNX(principal string, identity Identity) (map[string]*redis.BoolCmd, error) {
	pipe := rs.client.TxPipeline()

	wasSet := make(map[string]*redis.BoolCmd)
	hKey := rs.hash(principal)
	for fKey, value := range identity {
		res := pipe.HSetNX(hKey, fKey, value)
		wasSet[fKey] = res
	}

	_, err := pipe.Exec()
	return wasSet, err
}

func (rs *redisIStore) Del(principal string, keys []string) error {
	return rs.client.HDel(rs.hash(principal), keys...).Err()
}

func (rs *redisIStore) Get(principal string) (SerializedIdentity, error) {
	res, err := rs.client.HGetAll(rs.hash(principal)).Result()
	serialized := serialize(res)
	return serialized, err
}

func (rs *redisIStore) hash(principal string) string {
	return "identity:" + principal
}
