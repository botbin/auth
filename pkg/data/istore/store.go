package istore

// An IdentityStore facilitates persistent identity management.
type IdentityStore interface {
	Set(principal string, identity Identity) error
	SetNX(principal string, identity Identity) error
	Del(principal string, keys []string) error
	Get(prinicpal string) (SerializedIdentity, error)
}
