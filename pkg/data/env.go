package data

import (
	"os"

	"go.uber.org/zap"
)

// GetEnv fetches an environment variable, optionally providing a default value.
func GetEnv(key, defaultVal string, ifEmptySkipDefault bool) string {
	val := os.Getenv(key)
	if val == "" && !ifEmptySkipDefault {
		val = defaultVal
		zap.S().Warnw("missing env var "+key, "default", defaultVal)
	}
	return val
}
