package sign_test

import (
	"testing"
	"time"

	"gitlab.com/botbin/auth/pkg/data/istore"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/auth/pkg/sign"
)

// Tests for TokenAuthority
type TokenAuthorityTestSuite struct {
	suite.Suite
}

// Verify that the values in Token match what was actually encoded.
// This test shouldn't ever use GetClaims; we're testing the contract
// to other services.
func (ts *TokenAuthorityTestSuite) TestTokenMatch() {
	secret := "test"
	ta := sign.NewTokenAuthority(secret)
	sub, dat, username := 1, "dat", "tester"
	identity := istore.SerializedIdentity{"username": username}

	token, err := ta.CreateAccessToken(sub, dat, identity)
	ts.Nil(err)

	parser := new(jwt.Parser)
	decoded, _, err := parser.ParseUnverified(token.Value, jwt.MapClaims{})
	ts.Nil(err)

	claims := decoded.Claims.(jwt.MapClaims)
	ts.Equal(username, claims["username"].(string))
	// By default, jwt uses float64 for decoding. It's whatever.
	ts.Equal(float64(sub), claims["sub"].(float64))
	ts.Equal(dat, claims["dat"].(string))
	ts.Equal(token.RevokeID, claims["rid"].(string))
	ts.Equal(float64(token.Expires), claims["exp"].(float64))
}

// Ensure that GetClaims can read the JWT claims.
func (ts *TokenAuthorityTestSuite) TestGetClaims_Valid() {
	secret := "test"
	ta := sign.NewTokenAuthority(secret)

	sub, dat, username := 1, "dat", "tester"
	identity := istore.SerializedIdentity{"username": username}

	token, err := ta.CreateAccessToken(sub, dat, identity)
	ts.Nil(err)

	claims, err := ta.GetClaims(token.Value)
	ts.Nil(err)

	ts.Equal(username, claims["username"].(string))
	// By default, jwt uses float64 for decoding. It's whatever.
	ts.Equal(float64(sub), claims["sub"].(float64))
	ts.Equal(dat, claims["dat"].(string))
	ts.Equal(token.RevokeID, claims["rid"].(string))
	ts.Equal(float64(token.Expires), claims["exp"].(float64))
}

// Ensure that invalid tokens are rejected by GetClaims.
func (ts *TokenAuthorityTestSuite) TestGetClaims_Invalid() {
	secret := "test"
	ta := sign.NewTokenAuthority(secret)

	expiredTS := time.Now().Add(-time.Second).UTC().Unix()
	validTS := time.Now().Add(time.Second).UTC().Unix()

	jwts := []string{
		"test",
		"",
		ts.getJWT(secret, "", validTS),
		ts.getJWT(secret, "test", validTS),
		ts.getJWT(secret, sign.Issuer, expiredTS),
		ts.getJWT("", sign.Issuer, validTS),
		ts.getJWT(secret+secret, sign.Issuer, validTS),
	}

	for i, jwt := range jwts {
		claims, err := ta.GetClaims(jwt)
		ts.NotNil(err, "case %d producing %+v", i, claims)
	}
}

func (ts *TokenAuthorityTestSuite) getJWT(secret, iss string, exp int64) string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"iss": iss,
		"exp": exp,
	})
	encoded, err := token.SignedString([]byte(secret))
	ts.Nil(err)
	return encoded
}

// Ensure that all tokens get the standard claim map of:
//	iss, exp, sub, rid, dat
func (ts *TokenAuthorityTestSuite) TestStandardClaims() {
	ta := sign.NewTokenAuthority("test")

	identity := istore.SerializedIdentity{}
	token, err := ta.CreateAccessToken(1, "t", identity)
	ts.Nil(err)

	claims, err := ta.GetClaims(token.Value)
	ts.Nil(err)

	required := []string{"iss", "exp", "sub", "rid", "dat"}
	for _, key := range required {
		ts.NotEmpty(claims[key])
	}
}

// Ensure that 'rid' claims are properly seeded.
func (ts *TokenAuthorityTestSuite) TestRIDSeed() {
	ta := sign.NewTokenAuthority("test")
	tokA, err := ta.CreateAccessToken(0, "", istore.SerializedIdentity{})
	ts.Nil(err)

	ta = sign.NewTokenAuthority("test")
	tokB, err := ta.CreateAccessToken(0, "", istore.SerializedIdentity{})
	ts.Nil(err)

	ts.NotEqual(tokA.RevokeID, tokB.RevokeID)
}

func TestTokenAuthority(t *testing.T) {
	suite.Run(t, new(TokenAuthorityTestSuite))
}
