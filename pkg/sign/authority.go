package sign

import (
	"encoding/base64"
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/satori/go.uuid"
	"gitlab.com/botbin/auth/pkg/data/istore"
)

// TokenAuthority handles creation and parsing of JWTs.
type TokenAuthority struct {
	secret  []byte
	keyFunc jwt.Keyfunc
}

const (
	// AccessTokenDuration is the amount of time that access tokens remain valid.
	AccessTokenDuration = time.Hour * 24 * 7
	// Issuer is the JWT issuer.
	Issuer = "botbin"
)

var errInvalidToken = errors.New("failed to validate access token")

// NewTokenAuthority configures a TokenAuthority to perform
// token management with a given JWT signing secret.
func NewTokenAuthority(secret string) *TokenAuthority {
	jwtKey := []byte(secret)

	return &TokenAuthority{
		secret: jwtKey,
		keyFunc: func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}

			return jwtKey, nil
		},
	}
}

// GetClaims gets the claims from a JWT or returns an error if the token
// is no longer valid.
func (ta *TokenAuthority) GetClaims(token string) (jwt.MapClaims, error) {
	parsedToken, err := jwt.Parse(token, ta.keyFunc)
	if err != nil {
		return nil, err
	}

	claims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !(ok && parsedToken.Valid) {
		return nil, errInvalidToken
	}
	return claims, ta.check(claims)
}

func (ta *TokenAuthority) check(claims jwt.MapClaims) error {
	iss, exists := claims["iss"]
	if !exists {
		return errInvalidToken
	}

	issAsString, ok := iss.(string)
	if !ok {
		return errInvalidToken
	}

	if issAsString != Issuer {
		return errInvalidToken
	}
	return nil
}

// CreateAccessToken makes a new access token that will last for the duration
// specified by AccessTokenDuration.
func (ta *TokenAuthority) CreateAccessToken(principal int, discordAccessToken string, identity istore.SerializedIdentity) (Token, error) {
	expiresAtUnix := time.Now().UTC().Add(AccessTokenDuration).Unix()
	rid := ta.rand()

	claims := jwt.MapClaims{}
	for key, val := range identity {
		claims[key] = val
	}
	claims["exp"] = expiresAtUnix
	claims["sub"] = principal
	claims["iss"] = Issuer
	claims["rid"] = rid
	claims["dat"] = discordAccessToken

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return ta.wrapJWT(token, int(expiresAtUnix), rid)
}

func (ta *TokenAuthority) wrapJWT(token *jwt.Token, expiresAtUnix int, revokeID string) (Token, error) {
	tokenString, err := token.SignedString(ta.secret)
	if err != nil {
		return Token{}, err
	}

	return Token{
		Value:    tokenString,
		Expires:  expiresAtUnix,
		RevokeID: revokeID,
	}, nil
}

func (ta *TokenAuthority) rand() string {
	base := uuid.NewV4().Bytes()
	return base64.RawURLEncoding.EncodeToString(base)
}
