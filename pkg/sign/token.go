package sign

// Token is an expireable value that grants access to resources.
type Token struct {
	// Value is the encoded token.
	Value string `json:"accessToken"`

	// Expires is a Unix timestamp when the token expires.
	Expires int `json:"expiresAt"`

	// RevokeID helps identify tokens that are revoked.
	// This value should also be present in the claims of the
	// encoded token as 'rid'.
	RevokeID string `json:"-"`
}

// TokenPair groups an access token with the refresh token that created it.
type TokenPair struct {
	RefreshToken string
	AccessToken  Token
}

// RevokedToken identifies a token that has been revoked.
type RevokedToken struct {
	// ID is the revoke ID of the original token.
	// This value should be present in the claims of the
	// original token as 'rid'.
	ID string `json:"rid"`

	// Expires is a Unix timestamp indicating when the original
	// token will expire.
	Expires int `json:"expiresAt"`
}
