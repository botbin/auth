package oauth

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strings"
	"time"

	"gitlab.com/botbin/auth/pkg/sign"
	"gitlab.com/botbin/auth/pkg/data"
)

// discord is a Provider that handles Discord token acquisition.
type discord struct {
	AuthURL      string
	MeURL        string
	ClientID     string
	ClientSecret string
	RedirectURI  string
	Client       *http.Client
}

// Discord creates a Provider that is connected to Discord servers.
// This method relies on the environment variables
//		DISCORD_CLIENT_ID
//		DISCORD_CLIENT_SECRET
//		DISCORD_REDIRECT_URI
func Discord() Provider {
	return &discord{
		AuthURL:      "https://discordapp.com/api/v6/oauth2/token",
		MeURL:        "https://discordapp.com/api/v6/users/@me",
		ClientID:     data.GetEnv("DISCORD_CLIENT_ID", "", false),
		ClientSecret: data.GetEnv("DISCORD_CLIENT_SECRET", "", false),
		RedirectURI:  data.GetEnv("DISCORD_REDIRECT_URI", "http://localhost:3000/oauth2_callback", false),
		Client: &http.Client{
			Timeout: 5 * time.Second,
		},
	}
}

// ExchangeCode exchanges a Discord access code for token pair.
func (d *discord) ExchangeCode(code string) (sign.TokenPair, error) {
	req, err := d.buildTokenRequest("code", code, "authorization_code")
	if err != nil {
		return sign.TokenPair{}, err
	}

	res, err := d.Client.Do(req)
	if err != nil {
		return sign.TokenPair{}, err
	}
	defer res.Body.Close()

	return d.parseTokenResponse(res)
}

// ExchangeRefresh uses a Discord refresh token to get an access token.
func (d *discord) ExchangeRefresh(refreshToken string) (sign.Token, error) {
	req, err := d.buildTokenRequest("refresh_token", refreshToken, "refresh_token")
	if err != nil {
		return sign.Token{}, err
	}

	res, err := d.Client.Do(req)
	if err != nil {
		return sign.Token{}, err
	}
	defer res.Body.Close()

	tokens, err := d.parseTokenResponse(res)
	return tokens.AccessToken, err
}

func (d *discord) buildTokenRequest(payloadKey, val, grantType string) (*http.Request, error) {
	form := url.Values{
		"client_id":     []string{d.ClientID},
		"client_secret": []string{d.ClientSecret},
		"grant_type":    []string{grantType},
		payloadKey:      []string{val},
		"redirect_uri":  []string{d.RedirectURI},
	}

	req, err := http.NewRequest(http.MethodPost, d.AuthURL, strings.NewReader(form.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	return req, nil
}

// parseResponse parses token responses. If an access token was requested instead of a
// refresh token, the string in the response tuple will be blank.
func (d *discord) parseTokenResponse(r *http.Response) (sign.TokenPair, error) {
	if r.StatusCode != http.StatusOK && r.StatusCode != http.StatusCreated {
		return sign.TokenPair{}, errors.New("unexpected Discord response code " + r.Status)
	}

	var tokenResponse discordAccessTokenResponse
	json.NewDecoder(r.Body).Decode(&tokenResponse)

	return sign.TokenPair{
		RefreshToken: tokenResponse.RefreshToken,
		AccessToken: sign.Token{
			Value:   tokenResponse.AccessToken,
			Expires: tokenResponse.ExpiresIn,
		},
	}, nil
}

// Identify gets the Discord ID of an access token's creator.
func (d *discord) Identify(accessToken string) (string, error) {
	req, err := http.NewRequest(http.MethodGet, d.MeURL, nil)
	if err != nil {
		return "", err
	}
	req.Header.Set("Authorization", "Bearer "+accessToken)

	res, err := d.Client.Do(req)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return "", errors.New("failed to get user ID from Discord")
	}

	body := make(map[string]interface{})
	json.NewDecoder(res.Body).Decode(&body)
	return body["id"].(string), nil
}

// discordAccessTokenResponse models the token response from Discord.
type discordAccessTokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
}
