package oauth

import "gitlab.com/botbin/auth/pkg/sign"

// A Provider combines proxy methods for OAuth2 resource and API servers.
type Provider interface {
	ExchangeCode(code string) (sign.TokenPair, error)
	ExchangeRefresh(token string) (sign.Token, error)
	Identify(token string) (string, error)
}
