package token

import (
	"errors"
	"strconv"

	"gitlab.com/botbin/auth/pkg/data"
	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/service/oauth"
	"gitlab.com/botbin/auth/pkg/sign"
)

var (
	// ErrProvider401 signals that a provider responded to a token request with
	// a 401 status code.
	ErrProvider401 = errors.New("Provider rejected the provided refresh token")
)

// ATGenerator generates access tokens.
type ATGenerator struct {
	authority  *sign.TokenAuthority
	identities istore.IdentityStore
	tokens     data.TokenStore
	provider   oauth.Provider
	revoker    *Revoker
}

// NewAT configures an ATGenerator instance.
func NewAT(provider oauth.Provider,
	tokens data.TokenStore,
	identities istore.IdentityStore,
	auth *sign.TokenAuthority,
	revoker *Revoker) *ATGenerator {
	return &ATGenerator{
		authority:  auth,
		identities: identities,
		tokens:     tokens,
		provider:   provider,
		revoker:    revoker,
	}
}

// New exchanges a refresh token for an access token.
// The refresh token should belong to this service, not a provider.
func (atg *ATGenerator) New(refreshToken string) (sign.Token, error) {
	meta, err := atg.tokens.GetRefreshTokenDetails(refreshToken)
	if err != nil {
		return sign.Token{}, err
	}

	token, err := atg.create(meta, refreshToken)
	if err != nil {
		return sign.Token{}, err
	}

	err = atg.tokens.StoreAccessToken(meta.Principal, refreshToken, token)
	return token, err
}

func (atg *ATGenerator) create(meta data.RefreshTokenMeta, refreshToken string) (sign.Token, error) {
	provAccessToken, err := atg.provider.ExchangeRefresh(meta.DiscordRefreshToken)
	if err != nil {
		atg.revoker.RevokeRefresh(refreshToken)
		return sign.Token{}, ErrProvider401
	}
	return atg.NewInMemory(meta.Principal, provAccessToken.Value)
}

// NewInMemory creates an access token based on a principal and provider
// access token. It does not store the token.
func (atg *ATGenerator) NewInMemory(principal int, pTok string) (sign.Token, error) {
	identity, err := atg.identities.Get(strconv.Itoa(principal))
	if err != nil {
		return sign.Token{}, err
	}
	return atg.authority.CreateAccessToken(principal, pTok, identity)
}
