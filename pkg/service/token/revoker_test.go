package token_test

import (
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	tmocks "gitlab.com/botbin/auth/pkg/data/mocks"
	"gitlab.com/botbin/auth/pkg/event"
	pmocks "gitlab.com/botbin/auth/pkg/event/mocks"
	"gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/sign"
)

// Tests for Revoker
type RevokerTestSuite struct {
	suite.Suite
	store    *tmocks.TokenStore
	producer *pmocks.Producer
}

func (rs *RevokerTestSuite) SetupTest() {
	rs.store = new(tmocks.TokenStore)
	rs.producer = new(pmocks.Producer)
}

func (rs *RevokerTestSuite) TearDownTest() {
	rs.store.AssertExpectations(rs.T())
	rs.producer.AssertExpectations(rs.T())
}

// Ensure that GetAll retrieves all revoked tokens that the token store holds.
func (rs *RevokerTestSuite) TestGetAll_Trust() {
	exp := int(time.Now().UTC().Unix())
	expected := []sign.RevokedToken{
		sign.RevokedToken{ID: "a", Expires: exp},
		sign.RevokedToken{ID: "b", Expires: 0},
	}
	rs.store.On("GetRevokedTokens").Return(expected)

	revoker := token.NewRevoker(rs.store, token.TrustStore)
	actual := revoker.GetAll()

	rs.sort(expected)
	rs.sort(actual)
	rs.Equal(len(expected), len(actual))
	for i := range expected {
		rs.EqualValues(expected[i], actual[i])
	}
}

// Ensure that GetAll retrieves and prunes all revoked tokens that the token store holds.
func (rs *RevokerTestSuite) TestGetAll_Prune() {
	exp := int(time.Now().UTC().Unix())
	expected := []sign.RevokedToken{
		sign.RevokedToken{ID: "a", Expires: exp},
		sign.RevokedToken{ID: "b", Expires: 0},
	}
	rs.store.On("GetRevokedTokens").Return(expected)

	revoker := token.NewRevoker(rs.store, token.PruneExpired)
	actual := revoker.GetAll()

	rs.sort(expected)
	rs.sort(actual)
	rs.Equal(1, len(actual))
	rs.Equal(expected[0], actual[0])
}

func (rs *RevokerTestSuite) sort(a []sign.RevokedToken) {
	for i := 0; i < len(a)-1; i++ {
		j, x := i, &a[i]
		for j < len(a)-1 && x.ID > a[j+1].ID {
			j++
		}
		a[i], a[j] = a[j], a[i]
	}
}

// Ensure that revoking a refresh token results in revoked tokens being published
// as an event.
func (rs *RevokerTestSuite) TestRevokeRefresh_Published() {
	rs.runPublishedTest("RemoveRefreshToken", func(r *token.Revoker) error {
		return r.RevokeRefresh("test")
	})
}

// Ensure that revoking a principal's access tokens results in revoked tokens
// being published as an event.
func (rs *RevokerTestSuite) TestRevokeAccess_Published() {
	rs.runPublishedTest("RemoveAccessTokens", func(r *token.Revoker) error {
		return r.RevokeAccess("test")
	})
}

func (rs *RevokerTestSuite) runPublishedTest(method string, op func(*token.Revoker) error) {
	expected := []sign.RevokedToken{
		sign.RevokedToken{ID: "a", Expires: 1},
	}

	rs.store.On(method, mock.Anything).Return(nil, nil)
	rs.store.On("StoreRevokedAccessTokens", mock.Anything).Return(nil)
	rs.store.On("ToRevokedTokens", mock.Anything).Return(expected)

	rs.producer.On("Publish", mock.Anything).Run(func(args mock.Arguments) {
		revokedEvent := args[0].(*token.RevokedTokensEvent)

		var actual []sign.RevokedToken
		err := json.Unmarshal(revokedEvent.Body(), &actual)
		rs.Nil(err)

		rs.EqualValues(expected, actual)
	})

	event.Use(rs.producer)
	revoker := token.NewRevoker(rs.store, token.TrustStore)
	err := op(revoker)
	rs.Nil(err)
}

func TestRevoker(t *testing.T) {
	suite.Run(t, new(RevokerTestSuite))
}
