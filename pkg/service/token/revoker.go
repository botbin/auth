package token

import (
	"encoding/json"
	"time"

	"github.com/go-redis/redis"
	"gitlab.com/botbin/auth/pkg/data"
	"gitlab.com/botbin/auth/pkg/event"
	"gitlab.com/botbin/auth/pkg/sign"
	"go.uber.org/zap"
)

// Revoker manages invalidation of access and refresh tokens.
type Revoker struct {
	tokens data.TokenStore
	policy PrunePolicy
}

// NewRevoker configures a new Revoker instance.
func NewRevoker(store data.TokenStore, policy PrunePolicy) *Revoker {
	return &Revoker{
		tokens: store,
		policy: policy,
	}
}

// PrunePolicy specifies how to deal with collections of RevokedToken that
// may have expired items in them.
type PrunePolicy int

const (
	// PruneExpired directs a Revoker to check all collections of RevokedToken
	// for expired items and remove any if found.
	PruneExpired PrunePolicy = iota + 1

	// TrustStore directs a Revoker to trust that the store from which RevokedTokens
	// were retrieved took care of removing expired items. This option will
	// produce faster queries, but one must be sure that the provided
	// TokenStore handles expiration.
	TrustStore
)

// GetAll returns the collection of all unexpired revoked tokens.
func (r *Revoker) GetAll() []sign.RevokedToken {
	tokens := r.tokens.GetRevokedTokens()
	if r.policy == PruneExpired {
		tokens = r.getUnexpired(tokens)
	}
	return tokens
}

func (r *Revoker) getUnexpired(tokens []sign.RevokedToken) []sign.RevokedToken {
	now := int(time.Now().UTC().Unix())

	j := len(tokens)
	for i := 0; i < len(tokens) && i < j; {
		if tokens[i].Expires < now {
			j--
			tokens[i], tokens[j] = tokens[j], tokens[i]
		} else {
			i++
		}
	}
	// Create a window in the same slice that only includes unexpired tokens.
	tokens = tokens[:j]
	return tokens
}

// RevokeRefresh marks a refresh token and all access tokens it generated as invalid.
func (r *Revoker) RevokeRefresh(refreshToken string) error {
	revokedAccessTokens, err := r.tokens.RemoveRefreshToken(refreshToken)
	if err != nil {
		zap.S().Info("failed to revoke refresh token", "error", err)
		return err
	}

	r.logRevoked(revokedAccessTokens)
	return nil
}

// RevokeAccess marks all access tokens owned by a principal as invalid.
func (r *Revoker) RevokeAccess(principal string) error {
	revokedAccessTokens, err := r.tokens.RemoveAccessTokens(principal)
	if err != nil {
		zap.S().Info("failed to revoke access tokens", "error", err)
		return err
	}

	r.logRevoked(revokedAccessTokens)
	return nil
}

func (r *Revoker) logRevoked(revoked []redis.Z) error {
	err := r.tokens.StoreRevokedAccessTokens(revoked)
	if err != nil {
		return err
	}

	converted := r.tokens.ToRevokedTokens(revoked)
	if r.policy == PruneExpired {
		converted = r.getUnexpired(converted)
	}
	event.Publish(newRTE(converted))
	return nil
}

// RevokedTokensEvent is an Event that models a batch of revoked tokens.
type RevokedTokensEvent struct {
	body []byte
}

func newRTE(revoked []sign.RevokedToken) *RevokedTokensEvent {
	body, err := json.Marshal(revoked)
	if err != nil {
		zap.S().Errorw("failed to marshal revoked tokens", "error", err, "tokens", revoked)
		body = []byte("[]")
	}
	return &RevokedTokensEvent{body: body}
}

func (rte *RevokedTokensEvent) Destination() string {
	return "tokens.revoked"
}

// Body returns the event marshaled as JSON.
func (rte *RevokedTokensEvent) Body() []byte {
	return rte.body
}
