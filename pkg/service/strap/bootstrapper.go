package strap

import (
	"strconv"

	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/data/pstore"
	"gitlab.com/botbin/auth/pkg/service/oauth"
	"gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/sign"
)

// Bootstrapper creates new principals.
type Bootstrapper struct {
	provider   oauth.Provider
	principals pstore.PrincipalStore
	identities istore.IdentityStore
	refreshGen token.RTGenerator
}

// New configures a Bootstrapper instance.
func New(provider oauth.Provider,
	principals pstore.PrincipalStore,
	identities istore.IdentityStore,
	refreshGen token.RTGenerator) *Bootstrapper {
	return &Bootstrapper{
		provider:   provider,
		principals: principals,
		identities: identities,
		refreshGen: refreshGen,
	}
}

// NewPrincipal creates a new principal with an initial identity.
//
// It expects an OAuth2 code that can be exchanged with a provider using the
// ExchangeCode interface method. This means the code should not have been used
// prior to the call.
//
// This method does not overwrite existing data if the code belongs to an
// external account that is already paired with a principal. In that case,
// the identity is ignored and a valid pair is still returned.
func (b *Bootstrapper) NewPrincipal(code string, identity istore.Identity) (sign.TokenPair, error) {
	pair, err := b.provider.ExchangeCode(code)
	if err != nil {
		return sign.TokenPair{}, err
	}

	principal, err := b.getOrCreatePrincipal(pair.AccessToken.Value, identity)
	if err != nil {
		return sign.TokenPair{}, err
	}
	return b.refreshGen.New(pair, principal)
}

func (b *Bootstrapper) getOrCreatePrincipal(token string, identity istore.Identity) (int, error) {
	if principal, err := b.getPrincipal(token); err == nil {
		return principal, nil
	}
	return b.create(token, identity)
}

func (b *Bootstrapper) getPrincipal(providerAccessToken string) (int, error) {
	id, err := b.provider.Identify(providerAccessToken)
	if err != nil {
		return -1, err
	}
	return b.principals.Get(id)
}

func (b *Bootstrapper) create(token string, identity istore.Identity) (int, error) {
	idFromProvider, err := b.provider.Identify(token)
	if err != nil {
		return -1, err
	}

	principal, err := b.principals.Create(idFromProvider)
	if err != nil {
		return -1, err
	}

	err = b.identities.Set(strconv.Itoa(principal), identity)
	return principal, err
}
