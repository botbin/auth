package strap_test

import (
	"errors"
	"testing"

	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/service/strap"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	imocks "gitlab.com/botbin/auth/pkg/data/istore/mocks"
	pmocks "gitlab.com/botbin/auth/pkg/data/pstore/mocks"
	omocks "gitlab.com/botbin/auth/pkg/service/oauth/mocks"
	tmocks "gitlab.com/botbin/auth/pkg/service/token/mocks"
	"gitlab.com/botbin/auth/pkg/sign"
)

// Tests for Bootstrapper
type BootstrapperTestSuite struct {
	suite.Suite
	provider   *omocks.Provider
	principals *pmocks.PrincipalStore
	identities *imocks.IdentityStore
	generator  *tmocks.RTGenerator
}

func (bs *BootstrapperTestSuite) SetupTest() {
	bs.provider = new(omocks.Provider)
	bs.principals = new(pmocks.PrincipalStore)
	bs.identities = new(imocks.IdentityStore)
	bs.generator = new(tmocks.RTGenerator)
}

func (bs *BootstrapperTestSuite) TearDownTest() {
	bs.provider.AssertExpectations(bs.T())
	bs.principals.AssertExpectations(bs.T())
	bs.identities.AssertExpectations(bs.T())
	bs.generator.AssertExpectations(bs.T())
}

// Ensure that NewPrincipal fails if the OAuth provider rejects the code.
func (bs *BootstrapperTestSuite) TestNewPrincipal_ProviderFailure() {
	bs.provider.On("ExchangeCode", mock.Anything).Return(sign.TokenPair{}, errors.New(""))

	// nil is fine as long as we assert that provider returned the error.
	bootstrapper := strap.New(bs.provider, nil, nil, nil)
	_, err := bootstrapper.NewPrincipal("test", istore.Identity{})
	bs.NotNil(err)
}

// Ensure that the principal is created if they did not already exist.
func (bs *BootstrapperTestSuite) TestNewPrincipal_NotPresent() {
	providerAssignedId := "1"
	principalId := 4
	
	bs.provider.On("ExchangeCode", mock.Anything).Return(sign.TokenPair{}, nil)
	bs.provider.On("Identify", mock.Anything).Return(providerAssignedId, nil)
	bs.principals.On("Get", mock.Anything).Return(0, errors.New("not-exist"))
	bs.principals.On("Create", mock.Anything).Return(principalId, nil)
	bs.identities.On("Set", mock.Anything, mock.Anything).Return(nil)
	bs.generator.On("New", mock.Anything, principalId).Return(sign.TokenPair{}, nil)

	bootstrapper := strap.New(bs.provider, bs.principals, bs.identities, bs.generator)
	_, err := bootstrapper.NewPrincipal("test", istore.Identity{})
	bs.Nil(err)
}

// Ensure that the principal is not created if they existed -- but that the operation
// does not fail because of it.
func (bs *BootstrapperTestSuite) TestNewPrincipal_Present() {
	providerAssignedId := "1"
	principalId := 4
	
	bs.provider.On("ExchangeCode", mock.Anything).Return(sign.TokenPair{}, nil)
	bs.provider.On("Identify", mock.Anything).Return(providerAssignedId, nil)
	bs.principals.On("Get", mock.Anything).Return(principalId, nil)
	// It's important that the method gives New the previous principal id instead
	// of generating a new one, so don't use mock.Anything for that arg.
	bs.generator.On("New", mock.Anything, principalId).Return(sign.TokenPair{}, nil)

	bootstrapper := strap.New(bs.provider, bs.principals, bs.identities, bs.generator)
	_, err := bootstrapper.NewPrincipal("test", istore.Identity{})
	bs.Nil(err)
}

func TestBootstrapper(t *testing.T) {
	suite.Run(t, new(BootstrapperTestSuite))
}
