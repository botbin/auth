package event

import (
	"github.com/Shopify/sarama"
	"go.uber.org/zap"
)

// A Producer publishes events to a message broker.
type Producer interface {
	Publish(e Event)
}

type kafkaProducer struct {
	producer sarama.AsyncProducer
	input    chan<- *sarama.ProducerMessage
}

func newKafkaProducer(brokers []string) (Producer, error) {
	config := sarama.NewConfig()
	config.Producer.RequiredAcks = sarama.WaitForAll

	producer, err := sarama.NewAsyncProducer(brokers, config)
	if err != nil {
		return nil, err
	}

	prod := &kafkaProducer{producer: producer}
	prod.start()
	return prod, nil
}

func (kp *kafkaProducer) start() {
	kp.input = kp.producer.Input()
	go func() {
		for err := range kp.producer.Errors() {
			zap.S().Errorw("encountered error when publishing event", "error", err)
		}
	}()
}

func (kp *kafkaProducer) Publish(e Event) {
	kp.input <- &sarama.ProducerMessage{
		Topic: e.Destination(),
		Value: sarama.ByteEncoder(e.Body()),
	}
}
