package event

var pkgProducer Producer

// Use specifies which producer that Publish should use.
// Only one producer may be used at a time.
func Use(prod Producer) {
	pkgProducer = prod
}

// UseKafkaProducer sets the default producer to one that
// publishes to Kafka topics.
// Only one producer may be used at a time.
func UseKafkaProducer(brokers []string) error {
	producer, err := newKafkaProducer(brokers)
	if err == nil {
		Use(producer)
	}
	return err
}

// Publish sends an event to the configured producer.
func Publish(e Event) {
	pkgProducer.Publish(e)
}

// An Event specifies the state of something that others may
// want to know.
type Event interface {
	// Destination specifies where the event will go.
	// This will most likely be a topic or queue.
	Destination() string

	// Body specifies the contents of the event that
	// others will need to make sense of.
	Body() []byte
}
