package ident

import (
	"errors"

	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"go.uber.org/zap"
)

// IdentityController maps HTTP requests to principal identity management.
type IdentityController struct {
	revoker       *token.Revoker
	identityStore istore.IdentityStore
}

// URI is the URI for identity management.
const URI = "/v1/identity"

// NewController configures an IdentityController.
func NewController(identities istore.IdentityStore,
	revoker *token.Revoker) ctrl.Controller {
	return &IdentityController{
		revoker:       revoker,
		identityStore: identities,
	}
}

func (ic *IdentityController) Accept(router *gin.Engine) {
	router.POST(URI, ic.addIdentity)
	router.PUT(URI, ic.updateIdentity)
	router.DELETE(URI, ic.removeIdentity)
}

func (ic *IdentityController) addIdentity(ctx *gin.Context) {
	principal, err := ic.getPrincipal(ctx)
	if err != nil {
		ctx.JSON(400, gin.H{"message": err.Error()})
		return
	}

	ic.setIdentity(ctx, principal, ic.identityStore.SetNX)
}

func (ic *IdentityController) updateIdentity(ctx *gin.Context) {
	principal, err := ic.getPrincipal(ctx)
	if err != nil {
		ctx.JSON(400, gin.H{"message": err.Error()})
		return
	}

	ic.setIdentity(ctx, principal, ic.identityStore.Set)
}

func (ic *IdentityController) setIdentity(ctx *gin.Context, principal string, set func(string, istore.Identity) error) {
	vals := ctx.Request.URL.Query()
	delete(vals, "principal")
	id := istore.ToIdentity(vals)

	err := set(principal, id)
	if err != nil {
		ctx.JSON(400, gin.H{"message": "could not update identity"})
		zap.S().Infow("could not update identity", "principal", principal, "error", err)
		return
	}

	zap.S().Infow("updated identity table", "principal", principal, "new_vals", id)
}

func (ic *IdentityController) removeIdentity(ctx *gin.Context) {
	principal, err := ic.getPrincipal(ctx)
	if err != nil {
		ctx.JSON(400, gin.H{"message": err.Error()})
		return
	}

	err = ic.revoker.RevokeAccess(principal)
	if err != nil {
		zap.S().Errorw("failed to revoke access token", "principal", principal, "error", err)
		ctx.JSON(500, gin.H{
			"message": "failed to revoke existing access tokens; aborting operation",
		})
		return
	}

	keys := ic.getKeys(ctx)
	err = ic.identityStore.Del(principal, keys)
	if err != nil {
		zap.S().Infow("could not delete all keys from identity store",
			"principal", principal, "keys", keys, "error", err)
		ctx.JSON(400, gin.H{"message": "could not delete all keys"})
		return
	}

	zap.S().Infow("removed entries from identity table",
		"principal", principal,
		"keys", keys,
	)
}

func (ic *IdentityController) getKeys(ctx *gin.Context) []string {
	vals := ctx.Request.URL.Query()
	delete(vals, "principal")

	keys := make([]string, len(vals))
	i := 0
	for key := range vals {
		keys[i] = key
		i++
	}
	return keys
}

func (ic *IdentityController) getPrincipal(ctx *gin.Context) (string, error) {
	principal := ctx.Query("principal")
	if principal == "" {
		return "", errors.New("expected 'principal' query param")
	}
	return principal, nil
}
