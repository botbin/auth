package ctrl

import (
	"github.com/gin-gonic/gin"
)

// A Controller maps HTTP requests to service logic.
type Controller interface {
	Accept(r *gin.Engine)
}
