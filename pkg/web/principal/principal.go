package principal

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/service/strap"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"gitlab.com/botbin/auth/pkg/web/token"
	"go.uber.org/zap"
)

// PrincipalController handles HTTP requests to manage principals.
type PrincipalController struct {
	bootstrapper *strap.Bootstrapper
}

func NewController(bootstrapper *strap.Bootstrapper) ctrl.Controller {
	return &PrincipalController{
		bootstrapper: bootstrapper,
	}
}

func (pc *PrincipalController) Accept(router *gin.Engine) {
	router.POST("/v1/principals", pc.create)
}

func (pc *PrincipalController) create(ctx *gin.Context) {
	code := ctx.Query("code")
	vals := ctx.Request.URL.Query()
	delete(vals, "code")
	identity := istore.ToIdentity(vals)

	pair, err := pc.bootstrapper.NewPrincipal(code, identity)
	if err != nil {
		zap.S().Infow("failed to create principal", "error", err)
		ctx.JSON(400, gin.H{"message": "could not create principal"})
		return
	}

	ctx.JSON(201, token.TokenCreationResponse{
		RefreshToken: pair.RefreshToken,
		AccessToken:  pair.AccessToken.Value,
		ExpiresAt:    pair.AccessToken.Expires,
	})
}
