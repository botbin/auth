package revoked

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"go.uber.org/zap"
)

// RevokedTokenController handles HTTP requests regarding refresh token revocation.
type RevokedTokenController struct {
	revoker *token.Revoker
}

// URI is the URI for revoked tokens.
const URI = "/v1/tokens/revoked"

// NewController configures a RevokedTokenController.
func NewController(revoker *token.Revoker) ctrl.Controller {
	return &RevokedTokenController{
		revoker: revoker,
	}
}

func (rtc *RevokedTokenController) Accept(router *gin.Engine) {
	router.GET(URI, rtc.getRevoked)
	router.POST(URI, rtc.addRevoked)
}

func (rtc *RevokedTokenController) getRevoked(ctx *gin.Context) {
	revokedTokens := rtc.revoker.GetAll()
	ctx.JSON(200, revokedTokens)
}

func (rtc *RevokedTokenController) addRevoked(ctx *gin.Context) {
	refreshToken := ctx.Query("refresh_token")
	if refreshToken == "" {
		ctx.JSON(400, messageExpectedRefresh)
		return
	}

	err := rtc.revoker.RevokeRefresh(refreshToken)
	if err != nil {
		zap.S().Infow("failed to revoke refresh token", "error", err)
		ctx.JSON(400, messageRevokeFailed)
	} else {
		ctx.JSON(200, messageRevokeSucceeded)
	}
}

var (
	messageExpectedRefresh = gin.H{
		"message": "expected parameter 'refresh_token'",
	}

	messageRevokeFailed = gin.H{
		"message": "failed to revoke refresh token",
	}

	messageRevokeSucceeded = gin.H{
		"message": "token revoked successfully",
	}
)
