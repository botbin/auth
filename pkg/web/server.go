package web

import (
	"net/http"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/botbin/auth/pkg/data"
	"gitlab.com/botbin/auth/pkg/data/istore"
	"gitlab.com/botbin/auth/pkg/data/pstore"
	"gitlab.com/botbin/auth/pkg/service/oauth"
	"gitlab.com/botbin/auth/pkg/service/strap"
	tserv "gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/sign"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"gitlab.com/botbin/auth/pkg/web/health"
	"gitlab.com/botbin/auth/pkg/web/ident"
	"gitlab.com/botbin/auth/pkg/web/principal"
	"gitlab.com/botbin/auth/pkg/web/revoked"
	"gitlab.com/botbin/auth/pkg/web/token"
)

// Server defines the HTTP service.
type Server struct {
	Controllers []ctrl.Controller
}

// Start opens the HTTP server to connections.
func (s Server) Start(port string) error {
	router := gin.Default()
	for _, controller := range s.Controllers {
		controller.Accept(router)
	}
	return http.ListenAndServe(":"+port, router)
}

// NewServer configures and returns a Server instance that performs validation
// using cluster and Discord mechanisms.
func NewServer() (Server, error) {
	provider := oauth.Discord()
	tokens := data.NewRedisTokenStore()
	redis := data.GetRedisClient()
	identities := istore.New(redis)

	secret := data.GetEnv("JWT_KEY", uuid.NewV4().String(), false)
	auth := sign.NewTokenAuthority(secret)

	principals, err := pstore.New(redis, data.GetPGConn())
	if err != nil {
		return Server{}, err
	}

	revoker := tserv.NewRevoker(tokens, tserv.TrustStore)
	accessGen := tserv.NewAT(provider, tokens, identities, auth, revoker)
	refreshGen := tserv.NewRT(provider, tokens, principals, accessGen)
	bootstrapper := strap.New(provider, principals, identities, refreshGen)

	return Server{
		Controllers: []ctrl.Controller{
			health.NewController(),
			revoked.NewController(revoker),
			token.NewController(accessGen, refreshGen),
			ident.NewController(identities, revoker),
			principal.NewController(bootstrapper),
		},
	}, nil
}
