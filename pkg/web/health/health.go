package health

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/botbin/auth/pkg/data"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"gitlab.com/botbin/auth/pkg/web/health/check"
	"go.uber.org/zap"
)

// HealthCheckController handles HTTP requests for the service health.
type HealthCheckController struct {
	checkables check.List
}

// URI is the URI for health checks.
const URI = "/v1/healthz"

// NewController configures a HealthCheckController.
func NewController() ctrl.Controller {
	checkables := check.New()
	checkables.Add(redisCheckable{Client: data.GetRedisClient()})

	return &HealthCheckController{
		checkables: checkables,
	}
}

// NewCustomController creates a HealthCheckController with a custom list
// of checkables. This should only be used for testing. Refer to NewController
// for production use.
func NewCustomController(list check.List) ctrl.Controller {
	return &HealthCheckController{
		checkables: list,
	}
}

func (hcc *HealthCheckController) Accept(router *gin.Engine) {
	router.GET(URI, hcc.getHealth)
}

func (hcc *HealthCheckController) getHealth(ctx *gin.Context) {
	// TODO: We might want to find a status endpoint for Discord and check that.
	// TODO: Try to ping postgres.

	res, ok := hcc.checkables.Check()
	var status int
	if !ok {
		status = 500
		zap.S().Errorw("failed health check", "result", res)
	} else {
		status = 200
	}

	ctx.JSON(status, res)
}
