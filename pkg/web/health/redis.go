package health

import "github.com/go-redis/redis"

type redisCheckable struct {
	Client *redis.Client
}

func (rc redisCheckable) Check() error {
	return rc.Client.Ping().Err()
}

func (rp redisCheckable) Name() string {
	return "Redis"
}
