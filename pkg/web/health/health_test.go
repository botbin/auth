package health_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/botbin/auth/pkg/web/health"
	"gitlab.com/botbin/auth/pkg/web/health/check"
)

// HealthCheckControllerTestSuite tests HealthCheckController.
type HealthCheckControllerTestSuite struct {
	suite.Suite
}

// Handle should respond with 500 if the method is GET but Redis can't be reached.
func (hs *HealthCheckControllerTestSuite) TestHandle_RedisDisconnected() {
	list := check.New()
	list.Add(redisMock{DoesRespond: false})
	controller := health.NewCustomController(list)

	req, err := http.NewRequest("GET", health.URI, nil)
	assert.Nil(hs.T(), err)
	recorder := httptest.NewRecorder()

	router := gin.New()
	controller.Accept(router)
	router.ServeHTTP(recorder, req)

	assert.Equal(hs.T(), http.StatusInternalServerError, recorder.Code)
}

// Handle should respond with 200 if the method is GET and all components
// are working properly.
func (hs *HealthCheckControllerTestSuite) TestHandle_AllOK() {
	list := check.New()
	list.Add(redisMock{DoesRespond: true})
	controller := health.NewCustomController(list)

	req, err := http.NewRequest("GET", health.URI, nil)
	assert.Nil(hs.T(), err)
	recorder := httptest.NewRecorder()

	router := gin.New()
	controller.Accept(router)
	router.ServeHTTP(recorder, req)

	assert.Equal(hs.T(), http.StatusOK, recorder.Code)
}

type redisMock struct {
	DoesRespond bool
}

func (rm redisMock) Check() error {
	if rm.DoesRespond {
		return nil
	}
	return errors.New("mock")
}

func (rm redisMock) Name() string {
	return "redis mock"
}

func TestHealthCheckController(t *testing.T) {
	suite.Run(t, new(HealthCheckControllerTestSuite))
}
