package check

// A Checkable reports the health of some entity.
type Checkable interface {
	Check() error
	Name() string
}

// Result specifies the result of checking a Checkable.
type Result struct {
	Status  string
	Message string `json:"omitempty"`
}

const (
	statusUp   = "UP"
	statusDown = "DOWN"
)
