package check

import (
	"sync"
)

// A List maintains a list of Checkables that can all be checked together.
type List interface {
	Add(...Checkable)
	Check() (map[string]Result, bool)
}

type checkableList struct {
	checkables []Checkable
	mut        sync.RWMutex
}

func (cl *checkableList) Add(checkables ...Checkable) {
	cl.mut.Lock()
	defer cl.mut.Unlock()

	cl.checkables = append(cl.checkables, checkables...)
}

func (cl *checkableList) Check() (map[string]Result, bool) {
	cl.mut.RLock()
	defer cl.mut.RUnlock()

	res := make(map[string]Result)
	allOk := true

	for _, c := range cl.checkables {
		if err := c.Check(); err != nil {
			res[c.Name()] = Result{
				Status:  statusDown,
				Message: err.Error(),
			}
			allOk = false
		} else {
			res[c.Name()] = Result{
				Status: statusUp,
			}
		}
	}
	return res, allOk
}

// New returns a configured, thread-safe List.
func New() List {
	return &checkableList{}
}
