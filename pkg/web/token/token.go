package token

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/botbin/auth/pkg/service/token"
	"gitlab.com/botbin/auth/pkg/web/ctrl"
	"go.uber.org/zap"
)

// TokenController handles HTTP requests for refresh and access tokens.
type TokenController struct {
	accessGen  *token.ATGenerator
	refreshGen token.RTGenerator
}

// NewController configures a TokenController.
func NewController(a *token.ATGenerator, r token.RTGenerator) ctrl.Controller {
	return &TokenController{
		accessGen:  a,
		refreshGen: r,
	}
}

func (tc *TokenController) Accept(router *gin.Engine) {
	router.POST("/v1/tokens", tc.generateToken)
}

func (tc *TokenController) generateToken(ctx *gin.Context) {
	token := ctx.Query("refresh_token")
	if token != "" {
		tc.generateAccessToken(ctx, token)
		return
	}

	code := ctx.Query("code")
	if code != "" {
		tc.generateRefreshToken(ctx, code)
	} else {
		ctx.JSON(400, gin.H{
			"message": "missing parameter 'refresh_token' or 'code'",
		})
	}
}

func (tc *TokenController) generateAccessToken(ctx *gin.Context, refreshToken string) {
	accessToken, err := tc.accessGen.New(refreshToken)
	if err != nil {
		zap.S().Infow("could not grant access token", "error", err)
		ctx.JSON(401, gin.H{
			"message": "could not grant access token",
		})
		return
	}

	ctx.JSON(201, TokenCreationResponse{
		AccessToken: accessToken.Value,
		ExpiresAt:   accessToken.Expires,
	})
}

func (tc *TokenController) generateRefreshToken(ctx *gin.Context, code string) {
	pair, err := tc.refreshGen.NewFromCode(code)
	if err != nil {
		zap.S().Infow("could not grant refresh token", "error", err)
		ctx.JSON(401, gin.H{
			"message": "could not grant refresh token",
		})
		return
	}

	ctx.JSON(201, TokenCreationResponse{
		RefreshToken: pair.RefreshToken,
		AccessToken:  pair.AccessToken.Value,
		ExpiresAt:    pair.AccessToken.Expires,
	})
}
