package token

// TokenCreationResponse is the JSON response format for token requests.
type TokenCreationResponse struct {
	RefreshToken string `json:"refreshToken,omitempty"`
	AccessToken  string `json:"accessToken"`
	ExpiresAt    int    `json:"expiresAt"`
}
