/**
 * This schema was designed for PostgreSQL.
 *
 * The schema effectively belongs to this service, so changes
 * can be made as long as this service continues to fulfill
 * its API contracts.
 */

CREATE SCHEMA auth;

CREATE TABLE auth.principals (
    id serial PRIMARY KEY,
    discord_id text UNIQUE NOT NULL
);